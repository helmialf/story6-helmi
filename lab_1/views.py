from django.shortcuts import render
from .models import Input
from .forms import PostInput

def index(request):
	inputs = Input.objects.all()
	form = PostInput()
	if request.method == "POST":
		form = PostInput(request.POST)
		if form.is_valid():
			post = form.save(commit=False)
			post.author = request.user
			post.save()

			pass
	else:
		form = PostInput()

	context = {
	'judul_halaman':'List Proyek',
	'input':inputs,
	'form': form
	}
	return render(request, 'lab1.html', context)


def profil(request):
	return render(request,'profil.html')