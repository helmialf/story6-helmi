from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index,profil
from django.http import HttpRequest
from datetime import date
import unittest
from .models import Input
from selenium.webdriver.chrome.options import Options
from .forms import PostInput
from webdriver_manager.chrome import ChromeDriverManager
from selenium import webdriver
from django.test import LiveServerTestCase
# Create your tests here.


class Django_test(TestCase):

    def create_status(self, status = "test_status"):
        return Input.objects.create(status= status,)

    def test_model_create(self):
        test_model = self.create_status()
        self.assertTrue(isinstance(test_model, Input))

    def test_url_is_exist(self):
            response = Client().get('/')
            self.assertEqual(response.status_code, 200)
            #response = Client().get('/profil')
            #self.assertEqual(response.status_code, 200)

    def test_view_index_func(self):
            found = resolve('/')
            self.assertEqual(found.func, index)
            #found = resolve('/profil')
            #self.assertEqual(found.func, profil)


    def test_valid_form(self):
        form = PostInput(data={'status':'something',})
        self.assertTrue(form.is_valid())

    def test_invalid_form(self):
        form = PostInput(data={'status': '',})
        self.assertFalse(form.is_valid())
        self.assertEqual(
                form.errors['status'],
                ["This field is required."]
                )

    def test_post_success(self):
        text = 'something'
        response_post = Client().post('/', {'status': text})
        response = Client().get('/')
        html = response.content.decode('utf-8')
        self.assertIn('something', html)

    def test_post_failed(self):
        text = 'something'
        response_post = Client().post('/', {'status': ''})
        response = Client().get('/')
        html = response.content.decode('utf-8')
        self.assertNotIn('something', html)

    def test_contains_image(self):
        response = Client().get('/profil')
        html = response.content.decode('utf-8')
        #self.assertIn(<img src={% static "img/profil.jpg" %} alt="">,html)

        
class TestStory7(LiveServerTestCase):

  def setUp(self):
     chrome_options = Options()
     chrome_options.add_argument('--dns-prefetch-disable')
     chrome_options.add_argument('--no-sandbox')
     chrome_options.add_argument('--headless')
     chrome_options.add_argument('disable-gpu')
     #self.browser = webdriver.Chrome('./chromedriver',chrome_options=chrome_options)
     self.browser = webdriver.Chrome(ChromeDriverManager().install())
     #self.browser = webdriver.Chrome(executable_path=r'chromedriver.exe')

  def tearDown(self):
      self.browser.close()
      

  def test_status(self):
      driver = self.browser
      driver.get(('http://127.0.0.1:8000/'))

      status_form = driver.find_element_by_tag_name('form')
      status = status_form.find_element_by_id('id_status')

      status.send_keys('test oi')
      status_form.submit()


      self.assertEquals(driver.find_element_by_tag_name('h3').text, 'Status : test oi')

  def test_redirect_profile(self):
      driver = self.browser
      driver.get(('http://127.0.0.1:8000/'))

      a_link = driver.find_element_by_tag_name('a')
      a_link.click()

      driver_profil = self.browser
      self.assertEquals(driver.find_element_by_tag_name('h1').text, 'Helmi Al Farel')



  """"TEST BUAT CHALLENGE"""


  def test_css_property_text_color(self):
    driver = self.browser
    driver.get(('http://127.0.0.1:8000/'))

    text = driver.find_element_by_tag_name('h1')

    self.assertEquals(text.value_of_css_property('color'),'rgba(255, 0, 0, 1)')


  def test_css_property_font_size(self):
    driver = self.browser
    driver.get(('http://127.0.0.1:8000/'))
    text = driver.find_element_by_tag_name('h1')

    self.assertEquals(text.value_of_css_property('font-size'),"40px")

  def test_button_exist(self):
    driver = self.browser
    driver.get(('http://127.0.0.1:8000/'))
    element = driver.find_element_by_tag_name('button')

    self.assertIn(element.get_attribute("id"),"signupbtn ")


  def test_input_field_exist(self):
    driver = self.browser
    driver.get(('http://127.0.0.1:8000/'))

    element = driver.find_element_by_tag_name('input')

    self.assertIn(element.get_attribute("id"),"id_status ")