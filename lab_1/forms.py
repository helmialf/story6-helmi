from django import forms
from .models import Input

class PostInput(forms.ModelForm):

	class Meta:
		model = Input
		fields= ('status',)