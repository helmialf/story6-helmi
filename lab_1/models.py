from django.db import models
from django.utils import timezone

# Create your models here.
class Input(models.Model):
	status = models.CharField(max_length= 50)
	created_date = models.DateTimeField(default=timezone.now)